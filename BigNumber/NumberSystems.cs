﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace BigNumber
{
    class DecNumberSystems : ANumericSystem
    {
        override protected string modivityNumber(string _number) {
            return _number;
        }
    }

    class OctNumberSystems : ANumericSystem
    {
        override protected string modivityNumber(string _number)
        {
            BigInteger num_10 = BigInteger.Parse(_number);
            if (num_10 < 0) throw new ArgumentOutOfRangeException("val", "The parameter cannot be negative");
            if (num_10 == 0) return "0";
            string result = "";
            while (true)
            {
                BigInteger mod = num_10 % 8;
                BigInteger div = num_10 / 8;
                if (div < 8)
                {
                    result = result.Insert(0, mod.ToString());
                    result = result.Insert(0, div.ToString());
                    break;
                }
                num_10 = div;
                result = result.Insert(0, mod.ToString());
            }
            return BigInteger.Parse(result).ToString();
        }
    }
}
