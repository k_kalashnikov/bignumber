﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Numerics;

namespace BigNumber
{
    class Reflection
    {
        private string @namespace { get; set; }

        public Reflection(string _namespace) {
            @namespace = _namespace;
        }

        public void getIList()
        {
            var q = from t in Assembly.GetExecutingAssembly().GetTypes() where t.IsClass && t.Namespace == @namespace select t;
            TypeFilter myFilter = new TypeFilter(myInterfaceFilter);
            foreach (var item in q)
            {
                Console.WriteLine(item.FullName);
            }
        }

        public static bool myInterfaceFilter(Type typeObj, Object criteriaObj)
        {
            if (typeObj.ToString() == criteriaObj.ToString())
                return true;
            else
                return false;
        }
    }
}
