﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Reflection;

namespace BigNumber
{
    class Primary
    {
        private static Dictionary<string, ILanguage> LangDict = new Dictionary<string, ILanguage> {
            { "rus", new LanguageRus() },
            { "eng", new LanguageEng() }
        };

        private static Dictionary<string, ANumericSystem> SysDict = new Dictionary<string, ANumericSystem>
        {
            { "oct", new DecNumberSystems()},
            { "dec", new OctNumberSystems()}
        };

        private BigInteger number;

        public Primary(string[] args) {
            tryParams(args);
            tryCompliteParams(args);
            tryNumber(args);
        }

        public static void tryCompliteParams(string[] args) {
            bool comliteLang = false, comliteSys = false;
            foreach (var item in args)
            {
                if (SysDict.ContainsKey(item)) comliteSys = true;
                if (LangDict.ContainsKey(item)) comliteLang = true;
            }
            if (!comliteLang) throw new Exception("Сhoose the language please (example: -rus)");
            if (!comliteSys) throw new Exception("Select the number system (example: -oct)");
        }

        public bool isBadCmd(string cmd) {
            if (cmd.Equals("help"))
            {
                throw new Exception("This program displays the name of the number in words. \n [cmd] [options] [value] \n Options are mandatory.\n -rus :output the numbers in Russian \n -eng :output the numbers in English \n -dec the output of decimal numbers \n -oct conclusion the number of converted octal system");
            }
            if (SysDict.ContainsKey(cmd))
            {
                return true;
            }
            if (LangDict.ContainsKey(cmd))
            {
                return true;
            }
            BigInteger temp;
            if (BigInteger.TryParse(cmd, out temp))
            {
                return true;
            }
            return false;
        }

        public void tryParams(string[] args) {
            foreach (var item in args) {
                if (!isBadCmd(item)) {
                    throw new Exception("You entered an invalid parameter: " + item);
                }
            }
        }

        public void tryNumber(string[] args) {
            int count = 0;
            BigInteger temp = 0;
            foreach (string item in args)
            {
                if (BigInteger.TryParse(item, out temp))
                {
                    number = temp;
                    count++;
                }
            }
            if(count > 1) throw new Exception("In your request more than one number");
            if(count < 1) throw new ArgumentException("Your request for numbers is not detected");
        }

        public void Main(string[] args)
        {
            foreach (string item in args)
            {
                if (SysDict.ContainsKey(item))
                {
                    SysDict[item].Number = number.ToString();
                    SysDict[item].getOutput(args, LangDict);
                }
            }
            
        }
    }
}
