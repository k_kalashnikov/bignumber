﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace BigNumber
{
    class BigNumberConverter
    {

        private static List<string> getNumStruct(string _myStr)
        {
            List<string> result = new List<string>();
            string temp = "";
            for (int i = (_myStr.Length - 1), count = 0; i > -1; i--)
            {
                if (count == 3)
                {
                    result.Add(temp);
                    count = 0;
                    temp = "";
                }
                temp = temp.Insert(0, _myStr[i].ToString());
                count++;
            }
            result.Add(temp);
            return result;
        }

        public static string getNumberName(string _number, ILanguage _lang)
        {
            if (_number.Length >= 100)
                throw new ArgumentOutOfRangeException("_number", "The length of the string is outside the valid");

            if (BigInteger.Parse(_number) == 0)
                return _lang.Zero;

            string result = "";
            List<string> _strList = getNumStruct(_number);
            int index = 0;
            foreach (var item in _strList)
            {
                result = _lang.getNamePart(int.Parse(item), index++) + result;
            }
            return result;
        }
    }
}
