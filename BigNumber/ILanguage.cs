﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    interface ILanguage
    {
        string LangID { get; }
        string[] Hunds { get; }
        string[] Tens { get; }
        string[] Frac20 { get; }
        List<List<string>> Names { get; }
        string Zero { get; }
        string getNamePart(int val, int _numeric);
    }
}
