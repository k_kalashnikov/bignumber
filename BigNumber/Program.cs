﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace BigNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
                var tempArgs = args.Select(n => n.Trim('-')).ToArray();
                Primary p = new Primary(tempArgs);
                p.Main(tempArgs);
                /*Reflection r = new Reflection("BigNumber");
                r.getIList();*/
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
