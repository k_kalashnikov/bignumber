﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    abstract class ANumericSystem
    {
        private string number;

        public string Number
        {
            get
            {
                return number;
            }
            set
            {
                number = modivityNumber(value);
            }
        }


        public void getOutput(string[] args, Dictionary<string, ILanguage> _lang)
        {
            foreach (string item in args)
            {
                if (_lang.ContainsKey(item))
                {
                    Console.WriteLine(BigNumberConverter.getNumberName(Number, _lang[item]));
                }
            }
        }

        abstract protected string modivityNumber(string _number);
    }
}
