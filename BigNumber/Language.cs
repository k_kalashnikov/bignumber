﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    public class LanguageRus : ILanguage
    {

        public string LangID
        {
            get
            {
                return "rus";
            }
        }

        public string[] Hunds {

            get {
                return new string[] {"", "сто ", "двести ", "триста ", "четыреста ", "пятьсот ", "шестьсот ", "семьсот ", "восемьсот ", "девятьсот " };
            }
            
        }

        public string[] Tens
        {
            get {
                return new string[]{ "", "десять ", "двадцать ", "тридцать ", "сорок ", "пятьдесят ", "шестьдесят ", "семьдесят ", "восемьдесят ", "девяносто "};
            }

        }

        public string[] Frac20
        {

            get {
                return new string[] {"", "один ", "два ", "три ", "четыре ", "пять ", "шесть ",
                    "семь ", "восемь ","девять ", "десять ", "одиннадцать ", "двенадцать ",
                    "тринадцать ", "четырнадцать ", "пятнадцать ", "шестнадцать ", "семнадцать ", "восемнадцать ", "девятнадцать " };
            }
            
        }

        public List<List<string>> Names  {

            get
            {
                return new List<List<string>> {
                    new List<string> { "", "", "" },
                    new List<string>{ "тысяча", "тысячи", "тысяч" },
                    new List<string> { "миллион", "миллиона", "миллионов" },
                    new List<string> { "миллиард", "миллиарда", "миллиардов" },
                    new List<string> { "триллион", "триллиона", "триллионов" },
                    new List<string> { "квадриллион", "квадриллиона", "квадриллионов" },
                    new List<string> { "квинтиллион", "квинтиллиона", "квинтиллионов" },
                    new List<string> { "секстиллион", "секстиллиона", "секстиллионов" },
                    new List<string> { "септиллион", "септиллиона", "септиллионов" },
                    new List<string> { "октиллион", "октиллиона", "октиллионов" },
                    new List<string> { "нониллион", "нониллиона", "нониллионов" },
                    new List<string> { "дециллион", "дециллиона", "дециллионов" },
                    new List<string> { "андециллион", "андециллиона", "андециллионов" },
                    new List<string> { "дуодециллион", "дуодециллиона", "дуодециллионов" },
                    new List<string> { "тредециллион", "тредециллиона", "тредециллионов" },
                    new List<string> { "кваттордециллион", "кваттордециллиона", "кваттордециллионов" },
                    new List<string> { "квиндециллион", "квиндециллиона", "квиндециллионов" },
                    new List<string> { "квиндециллион", "квиндециллиона", "квиндециллионов" },
                    new List<string> { "сексдециллион", "сексдециллиона", "сексдециллионов" },
                    new List<string> { "септемдециллион", "септемдециллиона", "септемдециллионов" },
                    new List<string> { "октодециллион", "октодециллиона", "октодециллионов" },
                    new List<string> { "новемдециллион", "новемдециллиона", "новемдециллионов" },
                    new List<string> { "вигинтиллион", "вигинтиллиона", "вигинтиллионов" },
                    new List<string> { "анвигинтиллион", "анвигинтиллиона", "анвигинтиллионов" },
                    new List<string> { "анвиналлион", "анвиналлиона", "анвиналлионов" },
                    new List<string> { "дуовигинтиллион", "дуовигинтиллиона", "дуовигинтиллионов" },
                    new List<string> { "тревигинтиллион", "тревигинтиллиона", "тревигинтиллионов" },
                    new List<string> { "кватторвигинтиллион", "кватторвигинтиллиона", "кватторвигинтиллионов" },
                    new List<string> { "квинвигинтиллион", "квинвигинтиллиона", "квинвигинтиллионов" },
                    new List<string> { "сексвинатиллион", "сексвинатиллиона", "сексвинатиллионов" },
                    new List<string> { "сексвигинтиллион", "сексвигинтиллиона", "сексвигинтиллионов" },
                    new List<string> { "септемвигинтиллион", "септемвигинтиллиона", "септемвигинтиллионов" },
                    new List<string> { "октовигинтиллион", "октовигинтиллиона", "октовигинтиллионов" },
                    new List<string> { "новемвигинтиллион", "новемвигинтиллиона", "новемвигинтиллионов" },
                    new List<string> { "тригинтиллион", "тригинтиллиона", "тригинтиллионов" },
                    new List<string> { "антригинтиллион", "антригинтиллиона", "антригинтиллионов" },
                    new List<string> { "гугол", "гугола", "гуголов" }
                };
            }
            
        }

        public string Zero
        {
            get
            {
                return "ноль";
            }
        }

        public string getNamePart(int val, int _numeric)
        {
            int num = val % 1000;
            if (0 == num) return "";
            if (num < 0) throw new ArgumentOutOfRangeException("val", "The parameter cannot be negative");
            string[] frac20 = Frac20;
            frac20[1] = (_numeric == 1) ? "одна " : "один ";
            frac20[2] = (_numeric == 1) ? "две " : "два ";
            StringBuilder r = new StringBuilder(Hunds[num / 100]);
            if (num % 100 < 20)
            {
                r.Append(frac20[num % 100]);
            }
            else
            {
                r.Append(Tens[num % 100 / 10]);
                r.Append(frac20[num % 10]);
            }
            r.Append(Case(num, _numeric));
            if (r.Length != 0) r.Append(" ");

            return r.ToString();
        }

        private string Case(int val, int _numeric)
        {
            int t = (val % 100 > 20) ? val % 10 : val % 20;

            switch (t)
            {
                case 1: return Names[_numeric][0];
                case 2: case 3: case 4: return Names[_numeric][1];
                default: return Names[_numeric][2];
            }
        }
    }


    public class LanguageEng : ILanguage
    {

        public string LangID
        {
            get
            {
                return "eng";
            }
        }

        public string[] Hunds
        {

            get
            {
                return new string[] { "hungred ", "hungreds " };
            }

        }

        public string[] Tens
        {
            get
            {
                return new string[] {  "", "ten ", "twenty ", "thirty ", "forty ", "fifty ","sixty ", "seventy ", "eighty ", "ninety " };
            }

        }

        public string[] Frac20
        {

            get
            {
                return new string[] {
                     "", "one ", "two ", "three ", "four ", "five ", "six ",
                    "seven ", "eight ", "nine ", "ten ", "eleven ",
                    "twelve ", "thirteen ", "fourteen ", "fifteen ",
                    "sixteen ", "seventeen ", "eighteen ", "nineteen "
                };
            }
        }

        public List<List<string>> Names
        {

            get
            {
                return new List<List<string>> {
                    new List<string> { "", ""},
                    new List<string> { "thousand", "thousands"},
                    new List<string> { "Million", "Millions"},
                    new List<string> { "Milliard", "Milliards"},
                    new List<string> { "Trillion", "Trillions"},
                    new List<string> { "Quadrillion", "Quadrillions"},
                    new List<string> { "Quintillion", "Quintillions"},
                    new List<string> { "Sextillion", "Sextillions"},
                    new List<string> { "Septillion", "Septillions"},
                    new List<string> { "Octillion", "Octillions"},
                    new List<string> { "Nonillion", "Nonillions"},
                    new List<string> { "Decillion", "Decillions"},
                    new List<string> { "Undecillion", "Undecillions"},
                    new List<string> { "Duodecillion", "Duodecillions"},
                    new List<string> { "Tredecillion", "Tredecillions"},
                    new List<string> { "Quattuordecillion", "Quattuordecillions"},
                    new List<string> { "Quindecillion", "Quindecillions"},
                    new List<string> { "Sexdecillion", "Sexdecillions"},
                    new List<string> { "Septendecillion", "Septendecillions"},
                    new List<string> { "Octodecillion", "Octodecillions"},
                    new List<string> { "Novemdecillion", "Novemdecillions"},
                    new List<string> { "Vigintillion", "Vigintillions"},
                    new List<string> { "Centillion", "Centillions"},
                    new List<string> { "unvigintillion", "unvigintillions"},
                    new List<string> { "duovigintillion", "duovigintillions"},
                    new List<string> { "trevigintillion", "trevigintillions"},
                    new List<string> { "quattuorvigintillion", "quattuorvigintillions"},
                    new List<string> { "quinvigintillion", "quinvigintillions"},
                    new List<string> { "sexvigintillion", "sexvigintillions"},
                    new List<string> { "septenvigintillion", "septenvigintillions"},
                    new List<string> { "octovigintillion", "octovigintillions"},
                    new List<string> { "novemvigintillion", "novemvigintillions"},
                    new List<string> { "trigintillion", "trigintillions"},
                    new List<string> { "untrigintillion", "untrigintillions"},
                    new List<string> { "duotrigintillion", "duotrigintillions"},
                    new List<string> { "Googol", "Googols"},
                };
            }

        }

        public string Zero
        {
            get
            {
                return "zero";
            }
        }

        public string getNamePart(int val, int _numeric)
        {
            int num = val % 1000;
            if (0 == num) return "";
            if (num < 0) throw new ArgumentOutOfRangeException("val", "The parameter cannot be negative");

            StringBuilder r = new StringBuilder();
            if ((num / 100) > 0)
            {
                r.Append(Frac20[num / 100]);
                r.Append(((num / 100) > 1) ? Hunds[1] : Hunds[0]);
            }
            if (num % 100 < 20)
            {
                r.Append(Frac20[num % 100]);
            }
            else
            {
                r.Append(Tens[num % 100 / 10]);
                r.Append(Frac20[num % 10]);
            }
            r.Append((val > 1) ? Names[_numeric][1] : Names[_numeric][0]);
            if (r.Length != 0) r.Append(" ");

            return r.ToString();
        }
    }
}
